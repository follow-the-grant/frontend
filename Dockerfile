FROM node:14

VOLUME followthegrant/www/
WORKDIR followthegrant/
COPY package*.json ./
RUN npm install
COPY . .
ENV "ENV" "production"
CMD ["npm", "run", "build"]
