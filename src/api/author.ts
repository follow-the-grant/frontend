import service from './service'

function getAuthorArticles (author_id:string) {
  const params = {
    author_id: author_id,
  }

  return service.post('/rpc/author_articles', params)
}

function getAuthorCois (author_id:string) {
  const params = {
    author_id: author_id,
  }

  return service.post('/rpc/author_coi', params)
}

function getAuthors (page = 0, pageSize = 30) {
  let authorNum = page*pageSize,
      authorLastNum = authorNum + pageSize

  const config = {
    // headers: {
    //   range: `${authorNum}-${authorLastNum}`,
    //   prefer: 'count=exact'
    // },
    params: {
      limit: 20,
      offset: page
    }
  }

  return service.get('/contributor', config)
}

function searchAuthors (search_name = '') {
  let params = {
	'search_name': `plfts.${search_name}`
  }

  return service.get('/contributor', {'params': params})
}

export default {
  getAuthorArticles,
  getAuthorCois,
  getAuthors,
  searchAuthors
};


