import service from './service'

function getArticles (page = 0, pageSize = 20) {
  let articleNum = page*pageSize,
      articleLastNum = articleNum + pageSize

  const config = {
    headers: {
      range: `${articleNum}-${articleLastNum}`,
      prefer: 'count=exact'
    }
  }

  return service.get(`/article?select=*,coi(*)`, config)
}

function getArticlesWithAuthors (page = 0, pageSize = 20) {
  let articleNum = page*pageSize,
      articleLastNum = articleNum + pageSize

  const params = {
    "page": page,
    "page_size": pageSize	  
  }

  return service.post(`/rpc/articles_with_authors`, params)
}



function searchArticles (searchTitle = '', searchAbstract = '', dateOrder = '') {
  const params = {
    search_title: searchTitle,
    search_abstract: searchAbstract,
    date_order: 'desc'
  }
  return service.post(`/rpc/search_articles`, params)
}

export default {
  getArticles,
  getArticlesWithAuthors,
  searchArticles
};


