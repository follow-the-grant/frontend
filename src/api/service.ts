import axios from 'axios'

/* eslint-disable no-undef */
const service = axios.create({
  // baseURL: 'http://localhost:3002',
  baseURL: process.env.VUE_APP_API_URL,
  timeout: 15000 // request timeout
})

export default service

