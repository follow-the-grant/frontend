import service from './service'
import article from './article'
import author  from './author'
import coi  from './coi'
import institutions from './institutions'

export default {
  service,
  article,
  author,
  coi,
  institutions
};
