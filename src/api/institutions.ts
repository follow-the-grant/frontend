import service from './service'

function getResearchInstitutions (page = 0, pageSize = 30) {
  let institutionsNum = page*pageSize,
      institutionsLastNum = institutionsNum + pageSize

  const config = {
    // headers: {
    //   range: `${authorNum}-${authorLastNum}`,
    //   prefer: 'count=exact'
    // },
    params: {
      limit: 20,
      offset: page
    }
  }

  return service.get('/research_institution', config)
}

function searchResearchInstitutions (searchName = '') {
  const config = {
    // headers: {
    //   range: `${authorNum}-${authorLastNum}`,
    //   prefer: 'count=exact'
    // },
    params: {
      'name': `ilike.*${searchName}*`
    }
  }

  return service.get('/research_institution', config)
}


export default {
  getResearchInstitutions,
  searchResearchInstitutions
};


