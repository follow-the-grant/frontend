import service from './service'

function getCois (page = 0, pageSize = 30, withoutEmpty = false) {
  const params = {
      coi_text_search: '',
      lim: 20,
      pg: page,
      without_empty: withoutEmpty
    }

  return service.post('/rpc/search_coi', params)
}

function searchCois (searchCoiText = '', withoutEmpty = false, page = 0, pageSize = 30) {

  let params = {
    'coi_text_search': searchCoiText,
    'without_empty': withoutEmpty,
    lim: pageSize,
    pg: page
  }

  return service.post('/rpc/search_coi', params)
}

export default {
  getCois,
  searchCois
};


