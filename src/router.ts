import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Author from './views/Author.vue';
import Authors from './views/Authors.vue';
import Coi from './views/Coi.vue';
import Institutions from './views/Institutions.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Articles',
      component: Home,
    },
    {
      path: '/author/:author_id',
      name: 'author',
      component: Author,
    },
    {
      path: '/authors',
      name: 'authors',
      component: Authors,
    },
    {
      path: '/coi',
      name: 'coi',
      component: Coi,
    },
    {
      path: '/institutions',
      name: 'institutions',
      component: Institutions,
    },
  ],
});
